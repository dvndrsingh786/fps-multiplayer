﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviour
{
    [SerializeField] string player;


    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            GameObject a = PhotonNetwork.Instantiate(player, Vector3.zero, Quaternion.identity);
            a.GetComponent<FPSControllerLPFP.FpsControllerLPFP>().canWork = true;
        }
        else
        {
            GameObject a = PhotonNetwork.Instantiate(player, new Vector3(0,0,9), Quaternion.identity);
            a.GetComponent<FPSControllerLPFP.FpsControllerLPFP>().canWork = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

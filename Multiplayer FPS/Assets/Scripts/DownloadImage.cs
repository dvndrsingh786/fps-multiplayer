﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.IO;

public class DownloadImage : MonoBehaviour
{
    [SerializeField]string url = "https://www.howtogeek.com/wp-content/uploads/2018/06/shutterstock_1006988770.png";
    [SerializeField]RawImage yourRawImage;
    [SerializeField]TextureScale txtScl;
    public static DownloadImage dwnldImg;
    Texture2D downloadedTexture;

    void Start()
    {
        dwnldImg = this;
        StartCoroutine(DownloadImage1(url));
    }

    IEnumerator DownloadImage1(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
            yourRawImage.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        downloadedTexture = (Texture2D)yourRawImage.texture;
        downloadedTexture.filterMode = FilterMode.Point;
        SaveImage();
        TextureScale.Point(downloadedTexture, downloadedTexture.width / 5, downloadedTexture.height / 5);
        Invoke(nameof(SaveScaledImage), 2);
    }

    void SaveImage()
    {
        var bytes = downloadedTexture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/DownloadedImage.png", bytes);
        UnityEditor.AssetDatabase.Refresh();
    }

    void SaveScaledImage()
    {
        var bytes = downloadedTexture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/ScaledDownloadedImage.png", bytes);
        UnityEditor.AssetDatabase.Refresh();
    }
}

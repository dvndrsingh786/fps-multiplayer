﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class MainMenuHandler : MonoBehaviourPunCallbacks
{

    [SerializeField] RectTransform selectedText;
    int selectedAvatar = 0;
    [SerializeField] RectTransform[] avatars;
    RoomOptions opts;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Avatar"))
        {
            PlayerPrefs.SetInt("Avatar", -1);
        }
        selectedAvatar = PlayerPrefs.GetInt("Avatar");
        opts = new RoomOptions();
        opts.MaxPlayers = 2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetAvatar()
    {
        if (selectedAvatar == -1)
        {
            selectedText.gameObject.SetActive(false);
            return;
        }
        DavMaster.CopyRectTransform(avatars[selectedAvatar], selectedText);
        selectedText.gameObject.SetActive(true);
    }

    public void SelectAvatar(RectTransform clickedAvatar)
    {
        selectedText.gameObject.SetActive(true);
        DavMaster.CopyRectTransform(clickedAvatar, selectedText);
    }

    public void SaveAvatar(int avatarCode)
    {
        selectedAvatar = avatarCode;
    }

    public void GoNext()
    {
        PlayerPrefs.SetInt("Avatar", selectedAvatar);
    }

    #region Photon Function & Callbacks

    public void ConnectAndCreateRoom()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinOrCreateRoom("Interview", opts, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Main Game");
    }

    #endregion
}
